name := "processRecordsError"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies ++= {
  val akkaV = "2.4.17"
  Seq(
    "org.reactivemongo" %% "reactivemongo" % "0.12.5",
    "org.reactivemongo" %% "reactivemongo-akkastream" % "0.12.5",
    "com.typesafe.akka"   %%  "akka-stream"    % akkaV,
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "org.reactivemongo" %% "reactivemongo-play-json" % "0.12.5-play25",
    "com.typesafe.play"   %%  "play-json"     % "2.5.12",
    "org.slf4j" % "slf4j-api" % "1.7.21"
  )
}