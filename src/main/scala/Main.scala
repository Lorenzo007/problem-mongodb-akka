
import akka.stream.scaladsl.Sink
import reactivemongo.api.{MongoConnection, MongoDriver}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import play.api.libs.json.{JsObject, Json}
import reactivemongo.play.json._
import reactivemongo.play.json.collection.JSONCollection
import reactivemongo.akkastream.cursorProducer
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory

import scala.util.{Failure, Success}




object Main extends App {

  import scala.concurrent.ExecutionContext.Implicits.global

  implicit val config = ConfigFactory.load()
  implicit val system: ActorSystem = ActorSystem("system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val driver = MongoDriver()
  val parsedUri = MongoConnection.parseURI(config.getString("mongo.input.uri"))
  val connection = parsedUri.map(driver.connection)

  val futureConnection = Future.fromTry(connection)

  val collection = futureConnection.flatMap(_.database(config.getString("mongo.data.db")))
    .map(_.collection[JSONCollection](config.getString("mongo.data.collection")))

  val readyCollection = Await.result(collection, 10.seconds)

  val sourceOfProjects = readyCollection.find(Json.obj()).cursor[JsObject]().documentSource()

  val numF = sourceOfProjects.runWith(Sink.fold(0)((acc,_) => acc + 1))

  val done = sourceOfProjects.mapAsync(8) { o =>
    //some long computation
    Future(Thread.sleep(2000))
    //Future(Thread.sleep(10))
  }.runWith(Sink.fold(0)((acc,_) => {
     println(acc + 1)
      acc + 1
  }))


  done.onComplete {
    case Success(n) => {
      println(s"###########DONE $n RECORDS###########")
      println("the collection has: " + Await.result(numF, 20 seconds) + " documents")
    }
    case Failure(e) => throw e
   }


}
